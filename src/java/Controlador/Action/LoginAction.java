/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author ivan
 */
public class LoginAction extends org.apache.struts.action.Action {
    private Controlador.Form.Login Formulario;
    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception{
            Formulario =(Controlador.Form.Login) form;
            Modelo.Usuarios us=new Modelo.Usuarios();
            
            ActionForward retorno=new ActionForward();
            
            if((Formulario.getUsuario().equals(us.getUsuario()))&& (Formulario.getPassword().equals(us.getPassword())))
            retorno =mapping.findForward("welcome");
            else
            {
                ActionErrors errors=new ActionErrors();
                errors.add("Invalido",new ActionMessage("errors.invalido"));
                //almaceno el error
                saveErrors(request,errors);
                retorno=mapping.getInputForward();
            }
            return retorno;
    }
}
