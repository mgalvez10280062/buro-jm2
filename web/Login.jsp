<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="Estilo.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <html:html><h1 class="titulo">Login del sistema: buro de credito</h1></html:html>
        </body>
    <html:img src="Imagenes/logo.png"></html:img>
        <html:form action="/Login">
            <table border="1" class="datos_form">
                <tr><td>Usuario</td> <td><html:text property="usuario" styleClass="datos_form"></html:text></td></tr>
                <html:errors property="usuario"/><br/>
                <tr><td>Clave</td> <td><html:password property="password" styleClass="datos_form"></html:password></td></tr>
                        <html:errors property="password" /><br/>
                        <html:errors property="Invalido" /><br/>
            </table>
                        <html:submit value="Confirmar" styleClass="boton"></html:submit>
        </html:form>
    
</html>
